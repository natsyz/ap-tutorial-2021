package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    int countPowerPotensialFromBirthYear(int birthYear);
    String powerClassifier(int power);
}
